# PUBLIC APIs
[](https://mememaker.github.io/API/)

## SPRINTS
### Sprint1
- Entregar style guide
### Sprint2
- añadir depedencias DONE
- añadir colores, fondos DONE
- crear el launch screen DONE(but lack of right image)
- borrar el titulo de la app DONE
- crear los fragments y sus layouts
- crear recyclers views
## Utils
### escala memes: 16/9

## comentarios
- como puedo colocar las imagenes del submit( que pertenecen a una clase meme), sin llamar a la API
 para obtener el meme que contiene la imagen
- como nombrar los archivos layouts y .kt de los recycler view
- como hacer que se vea bien un contenedor de imagenes que tienen diferentes tamaños
- como evitar que el meme se vea mal cuando hay mucho texto
- colocar bordes al texto DONE

## BUGS
- al crear tu meme no se guarda el meme creado
- viewer: los botones de next y back se dañan en bajas resoluciones, SOLVED
- en el mobil la llamada a la api no funciona, NO APARECEN LOS MEMES, SOLVED
- los textos del edit text del viewer son transparentes.