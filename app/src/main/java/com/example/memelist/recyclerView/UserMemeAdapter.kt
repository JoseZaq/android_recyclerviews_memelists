package com.example.memelist.recyclerView

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.memelist.MainActivity
import com.example.memelist.R
import com.example.memelist.customView.StrokeTextView
import com.example.memelist.dataClass.Api.MemePackage
import com.example.memelist.dataClass.Submission
import com.example.memelist.fragment.UserFragmentDirections
import com.squareup.picasso.Picasso

class UserMemeAdapter:RecyclerView.Adapter<UserMemeAdapter.UserMemeAdapterViewHolder>() {
    private var submission = mutableListOf<Submission>()
    private var image = mutableListOf<String>()
    class UserMemeAdapterViewHolder(view : View): RecyclerView.ViewHolder(view){
        var img: ImageView
        var tvTopText: StrokeTextView
        var tvBottomText:StrokeTextView
        init{
            img = view.findViewById(R.id.iv_user_meme_image)
            tvTopText = view.findViewById(R.id.tv_user_top_text)
            tvBottomText = view.findViewById(R.id.tv_user_bottom_text)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserMemeAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_user_meme,parent,false)
        return UserMemeAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserMemeAdapterViewHolder, position: Int) {
        // convertir la url a una imagen bitmap y guardalo en el item
            holder.tvTopText.text = submission[position].topText
            holder.tvBottomText.text = submission[position].bottomText
        fun saveMeme(memePackage:MemePackage){
            image.set(position,memePackage.data.image)
            Picasso.get().load(memePackage.data.image).into(holder.img) // carga directamene la imagen en el xml
        }
        MainActivity.viewModel.callApiGetMemeById(submission[position].memeID,::saveMeme)
        // listeners
        holder.img.setOnClickListener {
            val direction = UserFragmentDirections.actionUserFragmentToImageFragment(submission[position].id,image[position])
            Navigation.findNavController(it).navigate(direction)
        }
    }

    override fun getItemCount(): Int {
        return submission.size
    }
    // getters and setters
    fun setMemes(memes: MutableList<Submission>){
        this.submission= memes
        image = mutableListOf()
        submission.forEach { image.add("") }
        notifyDataSetChanged()
    }

    fun getSubmissions(): MutableList<Submission>{
        return submission
    }
}