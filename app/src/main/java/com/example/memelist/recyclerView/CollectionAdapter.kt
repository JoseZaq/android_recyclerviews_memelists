package com.example.memelist.recyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.memelist.R
import com.example.memelist.dataClass.Api.MemeAPI
import com.example.memelist.fragment.GridFragmentDirections
import com.squareup.picasso.Picasso

class CollectionAdapter: RecyclerView.Adapter<CollectionAdapter.CollectionAdapterViewHolder>() {
    private var memes = mutableListOf<MemeAPI>()
    class CollectionAdapterViewHolder(view : View): RecyclerView.ViewHolder(view){
        var img: ImageView
        var tvTopText: TextView
        var tvBottomText:TextView
        init{
            img = view.findViewById(R.id.adapter_grid_img)
            tvTopText = view.findViewById(R.id.adapter_grid_topText)
            tvBottomText = view.findViewById(R.id.adapter_grid_bottomText)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_collection,parent,false)
        return CollectionAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CollectionAdapterViewHolder, position: Int) {
        // convertir la url a una imagen bitmap y guardalo en el item
        val imageURL = memes[position].image
        Picasso.get().load(imageURL).into(holder.img) // carga directamene la imagen en el xml
        holder.tvTopText.text = memes[position].topText
        holder.tvBottomText.text = memes[position].bottomText
        // listeners
        holder.img.setOnClickListener {
            val direction = GridFragmentDirections.actionGridFragmentToViewerFragment(memes[position].ID)
            Navigation.findNavController(it).navigate(direction)
        }
        //
        //
    }

    override fun getItemCount(): Int {
        return memes.size
    }
    // getters and setters
    fun setMemes(memes: MutableList<MemeAPI>){
        this.memes= memes
        notifyDataSetChanged()
    }
}