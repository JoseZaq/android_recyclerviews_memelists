package com.example.memelist.recyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.memelist.R
import com.example.memelist.dataClass.Api.SubmissionAPI
import com.squareup.picasso.Picasso

class SubmissionAdapter : RecyclerView.Adapter<SubmissionAdapter.GridAdapterViewHolder>(){
    private var submission = mutableListOf<SubmissionAPI>()
    private var urlImages = mutableListOf<String>()
    class GridAdapterViewHolder(view : View): RecyclerView.ViewHolder(view){
        var img: ImageView
        var tvTopText: TextView
        var tvBottomText: TextView
        init{
            img = view.findViewById(R.id.adapter_sub_img)
            tvTopText = view.findViewById(R.id.adapter_sub_topText)
            tvBottomText = view.findViewById(R.id.adapter_sub_BottomText)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_submission,parent,false)
        return GridAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridAdapterViewHolder, position: Int) {
        // convertir la url a una imagen bitmap y guardalo en el item
        val areEnoughMemes = position < urlImages.size - 1
        if(areEnoughMemes && urlImages[position] != "" ) {
            Picasso.get().load(urlImages[position]).into(holder.img) // carga directamene la imagen en el xml
            holder.tvTopText.text = submission[position].topText
            holder.tvBottomText.text = submission[position].bottomText
        }
        // listeners
//            holder.img.setOnClickListener {
//                val direction = GridFragmentDirections.actionGridFragmentToViewerFragment(submission[position].ID)
//                Navigation.findNavController(it).navigate(direction)
//            }
        //
        //
    }

    override fun getItemCount(): Int {
        return submission.size
    }
    // getters and setters
    fun setSubmissions(submissionAPI: MutableList<SubmissionAPI>){
        this.submission= submissionAPI
        notifyDataSetChanged()
    }
    fun setUrlImages(urlImages: MutableList<String>){
        this.urlImages = urlImages
        notifyDataSetChanged()
    }
}