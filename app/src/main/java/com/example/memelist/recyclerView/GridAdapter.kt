package com.example.memelist.recyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.memelist.R
import com.example.memelist.customView.StrokeTextView
import com.example.memelist.dataClass.Api.MemeAPI
import com.example.memelist.fragment.GridFragmentDirections
import com.squareup.picasso.Picasso

class GridAdapter : RecyclerView.Adapter<GridAdapter.GridAdapterViewHolder>() {
    private var memes = mutableListOf<MemeAPI>()
    class GridAdapterViewHolder(view : View): RecyclerView.ViewHolder(view){
        var img:ImageView
        var tvTopText:TextView
        var tvBottomText:StrokeTextView
        init{
            img = view.findViewById(R.id.adapter_grid_img)
            tvTopText = view.findViewById(R.id.adapter_grid_topText)
            tvBottomText = view.findViewById(R.id.adapter_grid_bottomText)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_grid,parent,false)
        return GridAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridAdapterViewHolder, position: Int) {
        // convertir la url a una imagen bitmap y guardalo en el item
        val imageURL = memes[position].image
        Picasso.get().load(imageURL).into(holder.img) // carga directamene la imagen en el xml
        holder.tvTopText.text = memes[position].topText
        holder.tvBottomText.text = memes[position].bottomText
        // listeners
        holder.img.setOnClickListener {
//            val direction = GridFragmentDirections.actionGridFragmentToViewerFragment(memes[position].ID)
//            Navigation.findNavController(it).navigate(direction)
        }
    }

    override fun getItemCount(): Int {
        return memes.size
    }
    // getters and setters
    fun setMemes(memes: MutableList<MemeAPI>){
        this.memes= memes
        notifyDataSetChanged()
    }
}