package com.example.memelist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import androidx.room.Room
import com.example.memelist.dataBase.DDBB.SubmissionDatabase
import com.example.memelist.dataClass.Api.MemeAPI
import com.example.memelist.utils.FileManager
import com.example.memelist.viewModels.MainViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    // vars
    companion object {
        lateinit var viewModel: MainViewModel
        lateinit var database: SubmissionDatabase
    }

    lateinit var navBb:BottomNavigationView
    //
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MemeList)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navBb = findViewById(R.id.bottomNavigationView)
        // view model
        viewModel= ViewModelProvider(this).get(MainViewModel::class.java)
        // database
        database = Room.databaseBuilder(this,
            SubmissionDatabase::class.java,
            "MemeDatabase").build()
        // load meme from local file
//        val memeArray = FileManager.readMemes(this.assets)
//        val memeList:MutableList<MemeAPI> = mutableListOf()
//        for (meme in memeArray) { memeList.add(meme.toSubmissionAPI()) }
//        viewModel.setMemes(memeList)
        // bottom menu
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        navBb.setupWithNavController(navController)
    }
}