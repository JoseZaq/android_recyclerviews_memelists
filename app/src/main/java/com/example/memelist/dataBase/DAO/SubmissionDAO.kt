package com.example.memelist.dataBase.DAO

import androidx.room.*
import com.example.memelist.dataBase.entities.SubmissionEntity
@Dao
interface SubmissionDAO {
    @Query("SELECT * FROM SubmissionEntity")
    fun getAllSubmissions():MutableList<SubmissionEntity>
    @Query("SELECT * FROM SubmissionEntity WHERE id=:id")
    fun getSubmissionById(id:Int):SubmissionEntity
    @Insert
    fun addSubmission(submissionEntity: SubmissionEntity)
    @Update
    fun updateSubmission(submissionEntity: SubmissionEntity)
    @Delete
    fun deleteSubmission(submissionEntity: SubmissionEntity)
}