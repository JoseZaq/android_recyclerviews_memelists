package com.example.memelist.dataBase.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "SubmissionEntity")
data class SubmissionEntity(
    @PrimaryKey(autoGenerate = true)
    var id:Int,
    val bottomText: String,
    val dateCreated: String,
    val memeID: Int,
    val topText: String
)
