package com.example.memelist.dataBase.DDBB

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.memelist.dataBase.DAO.SubmissionDAO
import com.example.memelist.dataBase.entities.SubmissionEntity

@Database(entities = arrayOf(SubmissionEntity::class),version = 1)
abstract class SubmissionDatabase:RoomDatabase() {
    abstract fun memeDao():SubmissionDAO
}