package com.example.memelist.customView

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import com.example.memelist.R

class StrokeTextView(context: Context,attrs: AttributeSet): androidx.appcompat.widget.AppCompatTextView(context,attrs,0) {
    val DEFAULT_STROKE_WIDTH = 1.0f

    private var _strokeWidth: Float = DEFAULT_STROKE_WIDTH
    private var _strokeColor: Int = Color.BLACK

    init{
        val a:TypedArray = context.obtainStyledAttributes(attrs, R.styleable.StrokeTextView,0,0)
        try{
            _strokeColor = a.getColor(R.styleable.StrokeTextView_strokeTextColor,currentTextColor)
            _strokeWidth = a.getFloat(R.styleable.StrokeTextView_strokeTextWidth,DEFAULT_STROKE_WIDTH)
        }finally {
            a.recycle()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        //set paint to fill mode
        val p: Paint = paint
        p.setStyle(Paint.Style.FILL)
        //draw the fill part of text
        super.onDraw(canvas)
        //save the text color
        val currentTextColor = currentTextColor
        //set paint to stroke mode and specify
        //stroke color and width
        p.setStyle(Paint.Style.STROKE)
        p.setStrokeWidth(_strokeWidth)
        setTextColor(_strokeColor)
        //draw text stroke
        super.onDraw(canvas)
        //revert the color back to the one
        //initially specified
        setTextColor(currentTextColor)
    }

    // getters + setters
    fun setStrokeTextColor(color: Int) {
        _strokeColor = color
    }

    fun setStrokeTextWidth(width: Float) {
        _strokeWidth = width
    }
}