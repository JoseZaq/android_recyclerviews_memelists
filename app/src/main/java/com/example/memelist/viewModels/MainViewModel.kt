package com.example.memelist.viewModels

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.memelist.MainActivity
import com.example.memelist.dataBase.entities.SubmissionEntity
import com.example.memelist.dataClass.Api.*
import com.example.memelist.dataClass.Submission
import com.example.memelist.io.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.reflect.KFunction1

class MainViewModel: ViewModel() {
    // vars
    private var memes = MutableLiveData<MutableList<MemeAPI>>()
    private var submissions = MutableLiveData<MutableList<SubmissionAPI>>()
    private var mines = MutableLiveData<MutableList<Submission>>()
    private var userMemesImage = mutableListOf<String>()
    private var userLVMemesImage = MutableLiveData<MutableList<String>>()
    private var ddbbSubmission = MutableLiveData<Submission>()
    private var api: ApiInterface
    // const
    init {
        memes.value = mutableListOf<MemeAPI>()
        api = ApiInterface.create()
        // updating api data
        updateMemeSection(1)
        updateSubmissions(1)
    }


    // API methods

    /**
     * returns 24 top memes from the api
     */
    private fun callApiGetMemeSection(section: Int){
        val apiMemes = api.getMemeSection(section) // metodo get para obtener la seccion 1 de memes
        apiMemes.enqueue(object : Callback<MemeSection> {
            override fun onFailure(call: Call<MemeSection>, t: Throwable) {
                Log.e("Error:",t.message.toString()) // muestra el error
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<MemeSection>,
                response: Response<MemeSection>
            ) {
                if(response.isSuccessful){
                    val body = response.body()
                    memes.postValue((body?.memes ?: mutableListOf())) // guardar los memes, en caso de ser nulo guardar una lista vacia
                }
            }
        })
    }

    private fun callApiGetMemeSubmission(section: Int){
        val apiSubmissions = api.getMemeSubmissionSection(section)
        apiSubmissions.enqueue(object: Callback<SubmissionSection>{
            override fun onResponse(call: Call<SubmissionSection>, response: Response<SubmissionSection>) {
                val body =  response.body()
                submissions.postValue(body?.submissionAPI ?: mutableListOf()) // save submissions from api
            }

            override fun onFailure(call: Call<SubmissionSection>, t: Throwable) {
                Log.e("ERROR:",t.message.toString())
            }

        } )
    }

    fun callApiGetMemeById(id: Int, onResponseFunction: KFunction1<MemePackage, Unit>){
        val apiMeme = api.getMeme(id)
        apiMeme.enqueue(object: Callback<MemePackage>{
            override fun onResponse(call: Call<MemePackage>, response: Response<MemePackage>) {
                val body = response.body()
                onResponseFunction(body!!)
            }

            override fun onFailure(call: Call<MemePackage>, t: Throwable) {
                Log.e("Error:",t.message.toString())
            }

        })
    }

    fun callAPIPostMeme(meme:MemeAPI, showFailureMessage: () -> Unit, showSuccessMessage:() -> Unit){
        val feedb = api.postMeme(meme)
        feedb.enqueue(object:Callback<MemeAPI>{
            override fun onResponse(call: Call<MemeAPI>, response: Response<MemeAPI>) {
                showSuccessMessage()
            }

            override fun onFailure(call: Call<MemeAPI>, t: Throwable) {
                showFailureMessage()
            }

        })
    }
    // dataBase methods
    fun addDDBBMeme(memeEntity:SubmissionEntity){
        viewModelScope.launch {
            withContext(Dispatchers.IO){ MainActivity.database.memeDao().addSubmission(memeEntity) }
        }
    }
    fun getDDBBMemes(){
        viewModelScope.launch {
            val response =  withContext(Dispatchers.IO){ MainActivity.database.memeDao().getAllSubmissions()}
            val _mines = mutableListOf<Submission>()
            response.forEach { subEntity ->
                _mines.add(Submission(subEntity.id,subEntity.bottomText,subEntity.dateCreated,subEntity.memeID,subEntity.topText))
            }
            mines.postValue(_mines)
        }
    }
    fun getDDBBSubmissionById(id:Int){
        viewModelScope.launch {
            val response =  withContext(Dispatchers.IO){ MainActivity.database.memeDao().getSubmissionById(id)}
            ddbbSubmission.postValue(Submission(response.id,response.bottomText,response.dateCreated,response.memeID,response.topText))
        }
    }
    // viewModel methods
    /**
     * update the section of memes calling the api
     */
    fun updateMemeSection(section: Int){
        callApiGetMemeSection(section)
    }

    private fun updateSubmissions(section: Int) {
        callApiGetMemeSubmission(section)
    }

    fun getMemeSection(): MutableList<MemeAPI>{
        return memes.value!!
    }
    fun getNextMeme(currentMeme: MemeAPI): MemeAPI {
        val indexOfCurrent = memes.value!!.indexOf(currentMeme)
        if(indexOfCurrent +1 >= memes.value!!.size)
            return currentMeme // return the same meme when there is no more memes back or next
        else
            return memes.value!!.get(indexOfCurrent+1) // return the meme after current
    }
    fun getPreviewMeme(currentMeme: MemeAPI): MemeAPI {
        val indexOfCurrent = memes.value!!.indexOf(currentMeme)
        if(indexOfCurrent - 1 < 0 )
            return currentMeme // return the same meme when there is no more memes back or next
        else
            return memes.value!![indexOfCurrent-1] // return the meme before current
    }

    fun getPreviewDdbbSubmission(currentSubmission: Submission):Submission{
        val indexOfCurrent = mines.value!!.indexOf(currentSubmission)
        if(indexOfCurrent - 1 < 0 )
            return currentSubmission // return the same meme when there is no more mines back or next
        else
            return mines.value!![indexOfCurrent-1] // return the meme before current
    }
    fun getNextDdbbSubmission(currentSubmission: Submission):Submission{
        val indexOfCurrent = mines.value!!.indexOf(currentSubmission)
        if(indexOfCurrent + 1 >= mines.value!!.indexOf(currentSubmission) )
            return currentSubmission // return the same meme when there is no more mines back or next
        else
            return mines.value!![indexOfCurrent+1] // return the meme before current
    }

    fun getMemeById(id: Int): MemeAPI? {
        return memes.value!!.find { it.ID == id }
    }
    fun getMemes(): MutableList<MemeAPI>? {
        return memes.value
    }
    fun setMemes(memes: MutableList<MemeAPI>){
        this.memes.postValue(memes)
    }

    fun getLiveDataMemes(): MutableLiveData<MutableList<MemeAPI>> {
        return memes
    }
    fun getLiveDateSubmissions(): MutableLiveData<MutableList<SubmissionAPI>> {
        return submissions
    }
    fun getLiveDateMines(): MutableLiveData<MutableList<Submission>> {
        return mines
    }
    fun getLiveDataDdbbSubmission():MutableLiveData<Submission>{
        return ddbbSubmission
    }
    fun addUserMemeImage(url:String){
        userMemesImage.add(url)
    }
    fun getUserMemeImage(): MutableList<String> {
        return userMemesImage
    }
    fun getLVUserMemeImage(): MutableLiveData<MutableList<String>> {
        return userLVMemesImage
    }
}