package com.example.memelist.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.memelist.MainActivity
import com.example.memelist.R
import com.example.memelist.dataClass.Api.MemeAPI
import com.example.memelist.dataClass.Api.MemePackage
import com.example.memelist.recyclerView.GridAdapter
import com.example.memelist.recyclerView.SubmissionAdapter

class HomeFragment : Fragment(R.layout.fragment_home) {
    // views
    lateinit var btAll:AppCompatButton
    lateinit var ivMemeOne: ImageView
    lateinit var ivMemeTwo: ImageView
    lateinit var ivMemeThree: ImageView
    lateinit var rvTopMemes: RecyclerView
    lateinit var rvSubmissions: RecyclerView
    //

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // views
        btAll = view.findViewById(R.id.bt_seeAll)

        rvTopMemes = view.findViewById(R.id.rv_top_memes) // creating rv
        val adapter = GridAdapter()
        rvTopMemes.adapter = adapter
        MainActivity.viewModel.getLiveDataMemes().observe(this.viewLifecycleOwner,{adapter.setMemes(it)})

        rvSubmissions = view.findViewById(R.id.rv_submissions)
        val adapterSub = SubmissionAdapter()
        rvSubmissions.adapter = adapterSub

        MainActivity.viewModel.getLiveDateSubmissions().observe(this.viewLifecycleOwner, {
                submissionList -> adapterSub.setSubmissions(submissionList)

            val urlImages = mutableListOf<String>()
            val apiMeme = MutableLiveData<MemeAPI>()
            apiMeme.observe(this.viewLifecycleOwner,{
                urlImages.add(apiMeme.value?.image ?: "")
                adapterSub.setUrlImages(urlImages) // save list of url images into adapter
            }) // save meme urlImage into a list
            submissionList.forEach { meme ->
                fun saveMeme(memePackage: MemePackage?){
                    apiMeme.postValue(memePackage?.data)
                }
                MainActivity.viewModel.callApiGetMemeById(meme.memeID,::saveMeme) // get meme by id
            }
        })

//        ivMemeOne = view.findViewById(R.id.iv_top_meme_1)
//        ivMemeTwo = view.findViewById(R.id.iv_top_meme_2)
//        ivMemeThree = view.findViewById(R.id.iv_top_meme_3)

//        MainActivity.viewModel.getLiveDataMemes().observe(this.viewLifecycleOwner,{
//            if(it.size !=0) {
//                Picasso.get().load(it?.get(0)?.image).into(ivMemeOne)
//                Picasso.get().load(it?.get(1)?.image).into(ivMemeTwo)
//                Picasso.get().load(it?.get(2)?.image).into(ivMemeThree)
//            }
//        }) // save api memes in adapter when thread call api finished

        // listeners
        btAll.setOnClickListener {
            findNavController().navigate(R.id.gridFragment)
        }
    }
}