package com.example.memelist.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.memelist.MainActivity
import com.example.memelist.R
import com.example.memelist.recyclerView.CollectionAdapter


/**
 * A simple [Fragment] subclass.
 * Use the [GridFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GridFragment : Fragment(R.layout.fragment_grid) {
    // vars
    lateinit var rvGrid: RecyclerView
    val viewModel = MainActivity.viewModel
    lateinit var adapter: CollectionAdapter
    //
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // recyclerView
        rvGrid =  view.findViewById(R.id.recycler_view_grid)
        adapter = CollectionAdapter()
        rvGrid.adapter = adapter
        viewModel.getLiveDataMemes().observe(viewLifecycleOwner,{adapter.setMemes(it)})
    }
}