package com.example.memelist.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.*
import androidx.navigation.Navigation
import com.example.memelist.MainActivity
import com.example.memelist.R
import com.example.memelist.dataClass.Api.MemeAPI
import com.squareup.picasso.Picasso

/**
 * A simple [Fragment] subclass.
 * Use the [ViewerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ViewerFragment : Fragment(R.layout.fragment_viewer) {
    // var
    val viewModel = MainActivity.viewModel
    // get meme
    lateinit var meme : MemeAPI
    lateinit var tvName: TextView
    lateinit var btBack: Button
    lateinit var btNext: Button
    lateinit var ivMeme: ImageView
    lateinit var etTopText:EditText
    lateinit var etBottomText:EditText
    lateinit var btMake:Button
    //
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // meme
        val memeId = arguments?.getInt("meme_id") // get parameter from navigation
        meme= viewModel.getMemeById(memeId!!)!!
        // views
        tvName = view.findViewById(R.id.tv_meme_name)
        btBack = view.findViewById(R.id.bt_back)
        btNext = view.findViewById(R.id.tv_next)
        ivMeme = view.findViewById(R.id.iv_view_meme)
        etTopText = view.findViewById(R.id.topEditText)
        etBottomText = view.findViewById(R.id.bottomEditText)
        btMake = view.findViewById(R.id.bt_make_meme)

        tvName.text = meme.name
        Picasso.get().load(meme.image).into(ivMeme) // carga la imagen en el imageView
        // listeners
        btBack.setOnClickListener{
            meme = viewModel.getPreviewMeme(meme)
            tvName.text = meme.name
            // update
            Picasso.get().load(meme.image).into(ivMeme) // carga la imagen en el imageView
        }
        btNext.setOnClickListener{
            meme = viewModel.getNextMeme(meme)
            // update
            tvName.text = meme.name
            Picasso.get().load(meme.image).into(ivMeme) // carga la imagen en el imageView
        }
        btMake.setOnClickListener{

            val direction = ViewerFragmentDirections.actionViewerFragmentToFinishFragment(meme.ID,
                arrayOf(etTopText.text.toString(),etBottomText.text.toString()))
            Navigation.findNavController(it).navigate(direction)

            /*
            if(etBottomText.text?.length == 0 && etTopText.text?.length == 0){
                val emptyTextMessage:Toast =
                    Toast.makeText(this.context,"write something funny first ",Toast.LENGTH_SHORT)
                emptyTextMessage.show()
            }else{
                val topText = etTopText.text
                val bottomText = etBottomText.text
                val newMeme =
                    MemeEntity(0, bottomText.toString(),meme.image,meme.name,meme.rank,meme.tags,topText.toString()) //cual id poner?
                MainActivity.viewModel.addDDBBMeme(newMeme)
            }
            */
        }

    }
}