package com.example.memelist.fragment

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.memelist.MainActivity
import com.example.memelist.R
import com.example.memelist.dataClass.Api.MemeAPI
import com.example.memelist.dataClass.Api.SubmissionAPI
import com.example.memelist.dataClass.Submission
import com.squareup.picasso.Picasso

class ImageFragment:Fragment(R.layout.fragment_image) {
    // var
    val viewModel = MainActivity.viewModel

    // get meme
    lateinit var submission: Submission
    lateinit var tvName: TextView
    lateinit var btBack: Button
    lateinit var btNext: Button
    lateinit var ivMeme: ImageView
    lateinit var tvTopText: TextView
    lateinit var tvBottomText: TextView

    lateinit var memeImage:String

    //
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // submission
        val submissionId = arguments?.getInt("meme_id") // get parameter from navigation
        memeImage = arguments?.getString("image")!!
        val lvSubmission = viewModel.getLiveDataDdbbSubmission()
        lvSubmission.observe(this.viewLifecycleOwner,{ submission ->
            this.submission = submission
            updateMeme()
        })
        viewModel.getDDBBSubmissionById(submissionId!!)

        // views
        tvName = view.findViewById(R.id.tv_image_meme_name)
        btBack = view.findViewById(R.id.bt_image_back)
        btNext = view.findViewById(R.id.tv_image_next)
        ivMeme = view.findViewById(R.id.iv_image_meme_image)
        tvTopText = view.findViewById(R.id.tv_image_top_text)
        tvBottomText = view.findViewById(R.id.tv_image_bottom_text)
        // listeners
        btBack.setOnClickListener {
            submission = viewModel.getPreviewDdbbSubmission(submission)
            updateMeme()
        }
        btNext.setOnClickListener {
            submission = viewModel.getNextDdbbSubmission(submission)
            updateMeme()
        }
    }

    private fun updateMeme() {
//        tvName.text = submission.name
        tvTopText.text = submission.topText
        tvBottomText.text = submission.bottomText
        Picasso.get().load(memeImage).into(ivMeme) // carga la imagen en el imageView
    }
}