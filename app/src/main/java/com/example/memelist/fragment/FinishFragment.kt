package com.example.memelist.fragment

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.memelist.MainActivity
import com.example.memelist.R
import com.example.memelist.dataBase.entities.SubmissionEntity
import com.example.memelist.dataClass.Api.MemeAPI
import com.squareup.picasso.Picasso
import java.time.LocalDateTime

class FinishFragment: Fragment(R.layout.fragment_finish) {
    // vars
    val viewModel = MainActivity.viewModel
    lateinit var meme : MemeAPI
    lateinit var btUpload: Button
    lateinit var btSave: Button
    lateinit var ivMeme: ImageView
    lateinit var tvTopText: TextView
    lateinit var tvBottomText: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // nav argument
        val memeId = arguments?.getInt("meme_id") // get parameter from navigation
        val memeText = arguments?.getStringArray("meme_text")!!
        meme = viewModel.getMemeById(memeId!!)!!
        // views
        btUpload = view.findViewById(R.id.bt_upload)
        btSave = view.findViewById(R.id.bt_save)
        ivMeme = view.findViewById(R.id.iv_meme_image)
        tvTopText = view.findViewById(R.id.tv_fin_top_text)
        tvBottomText = view.findViewById(R.id.tv_fin_bottom_text)

        Picasso.get().load(meme.image).into(ivMeme)
        tvTopText.text = memeText[0]
        tvBottomText.text = memeText[1]
        // listeners
        btUpload.setOnClickListener {
            viewModel.callAPIPostMeme(meme,::errorMessage ,::successMessage)
            addMemeToDDBB()
            Navigation.findNavController(it).navigate(FinishFragmentDirections.actionFinishFragmentToHomeFragment())
        }
        btSave.setOnClickListener {
            addMemeToDDBB()
            Navigation.findNavController(it).navigate(FinishFragmentDirections.actionFinishFragmentToHomeFragment())
        }
    }

    private fun addMemeToDDBB() {
        val topText = tvTopText.text
        val bottomText = tvBottomText.text
        val newMeme =
            SubmissionEntity(0,bottomText.toString(),LocalDateTime.now().toString(),meme.ID,topText.toString()) //cual id poner?
        viewModel.addUserMemeImage(meme.image)
        viewModel.addDDBBMeme(newMeme)
    }

    fun errorMessage(){
        val uploadErrorMessage:Toast =
            Toast.makeText(this.context,"meme upload failed, try later",Toast.LENGTH_SHORT)
        uploadErrorMessage.show()
    }

    fun successMessage(){
        val goodUploadMessage: Toast =
            Toast.makeText(this.context,"meme uploaded",Toast.LENGTH_SHORT)
        goodUploadMessage.show()
    }
}