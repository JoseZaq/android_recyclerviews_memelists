package com.example.memelist.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.memelist.MainActivity
import com.example.memelist.R
import com.example.memelist.dataClass.Api.MemeAPI
import com.example.memelist.dataClass.Api.MemePackage
import com.example.memelist.recyclerView.UserMemeAdapter

class UserFragment:Fragment(R.layout.fragment_user) {
    // vars
    lateinit var rvFav:RecyclerView
    lateinit var rvMine:RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // views
        rvFav = view.findViewById(R.id.rv_fav)
        rvMine = view.findViewById(R.id.rv_mine)
        //
        val rvFavAdapter = UserMemeAdapter()
        val rvMineAdapter = UserMemeAdapter()
        rvFav.adapter = rvFavAdapter
        rvMine.adapter = rvMineAdapter
        //
        MainActivity.viewModel.getDDBBMemes()
        MainActivity.viewModel.getLiveDateMines().observe(this.viewLifecycleOwner, { submissionList ->
            rvMineAdapter.setMemes(submissionList)
        })
    }
}