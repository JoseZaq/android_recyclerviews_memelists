package com.example.memelist.io

import com.example.memelist.dataClass.Api.MemeAPI
import com.example.memelist.dataClass.Api.MemePackage
import com.example.memelist.dataClass.Api.MemeSection
import com.example.memelist.dataClass.Api.SubmissionSection
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiInterface {
    companion object{
        val BASE_URL = "http://alpha-meme-maker.herokuapp.com/"
        fun create():ApiInterface{
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
    // services
    @GET("memes/{id}")
    fun getMeme(@Path("id") id:Int): Call<MemePackage>

    @GET("{section}")
    fun getMemeSection(@Path("section") section: Int): Call<MemeSection>

    @GET("submissions/{section}")
    fun getMemeSubmissionSection(@Path("section") section: Int): Call<SubmissionSection>

    @POST("http://alpha-meme-maker.herokuapp.com/submissions")
    fun postMeme(@Body meme: MemeAPI): Call<MemeAPI>
}