package com.example.memelist.dataClass.Api


import com.google.gson.annotations.SerializedName

data class SubmissionAPI(
    @SerializedName("bottomText")
    val bottomText: String,
    @SerializedName("dateCreated")
    val dateCreated: String,
    @SerializedName("memeID")
    val memeID: Int,
    @SerializedName("topText")
    val topText: String
)