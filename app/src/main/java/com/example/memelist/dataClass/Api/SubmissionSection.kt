package com.example.memelist.dataClass.Api


import com.google.gson.annotations.SerializedName

data class SubmissionSection(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val submissionAPI: MutableList<SubmissionAPI>,
    @SerializedName("message")
    val message: String,
    @SerializedName("next")
    val next: String
)