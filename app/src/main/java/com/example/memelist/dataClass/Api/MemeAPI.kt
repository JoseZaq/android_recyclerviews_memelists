package com.example.memelist.dataClass.Api

data class MemeAPI(
    val ID: Int,
    val bottomText: String?,
    val image: String,
    val name: String,
    val rank: Int?,
    val tags: String?,
    val topText: String
)