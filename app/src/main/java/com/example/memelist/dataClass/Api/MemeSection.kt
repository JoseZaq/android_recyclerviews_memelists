package com.example.memelist.dataClass.Api


import com.google.gson.annotations.SerializedName

data class MemeSection(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val memes: MutableList<MemeAPI>,
    @SerializedName("message")
    val message: String,
    @SerializedName("next")
    val next: String
)