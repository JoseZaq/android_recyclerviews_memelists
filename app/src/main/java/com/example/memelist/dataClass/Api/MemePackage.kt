package com.example.memelist.dataClass.Api

data class MemePackage(val code:Int, val data: MemeAPI, val message: String)
