package com.example.memelist.dataClass

import com.example.memelist.dataBase.entities.SubmissionEntity
import com.example.memelist.dataClass.Api.SubmissionAPI

class Submission(
    val id:Int,
    val bottomText: String,
    val dateCreated: String,
    val memeID: Int,
    val topText: String
){
    fun toSubmissionAPI():SubmissionAPI{
        return SubmissionAPI(bottomText,dateCreated,memeID,topText)
    }
    fun toSubmissionEntity():SubmissionEntity{
        return SubmissionEntity(id,bottomText,dateCreated,memeID,topText)
    }
}
