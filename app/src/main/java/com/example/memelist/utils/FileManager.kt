package com.example.memelist.utils

import android.content.res.AssetManager
import com.example.memelist.dataClass.Submission
import com.google.gson.Gson
import java.lang.StringBuilder


class FileManager {
    companion object {
        fun readMemes(assetManager: AssetManager): Array<Submission> {
            // gson
            val gson = Gson()
            // file
            val reader = assetManager.open("memes.json")
            // readers
            val fileString = StringBuilder()
            var i: Int
            while (reader.read().also { i = it } != -1) {
                fileString.append(i.toChar())
            }
            // save in object Product
            val submissionArray: Array<Submission> = gson.fromJson<Array<Submission>>(
                fileString.toString(),
                Array<Submission>::class.java
            )
            return submissionArray
        }
    }
}